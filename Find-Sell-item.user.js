// ==UserScript==
// @name         Travian 寻找自己正在卖出的物品
// @namespace    https://www.zxtra.cn/
// @version      0.2
// @description  try to find who bought you items!
// @author       XI
// @match        https://*.travian.com/hero/auction*
// @run-at document-end
// ==/UserScript==

(function () {
  "use strict";

  //数字时间戳转换成日期时间函数，time为传入的数字时间戳，如果数字时间戳先前作了/1000运算，请先*1000再传入
  function changeTimeFormat(time) {
    var date = new Date(time);
    var month =
      date.getMonth() + 1 < 10
        ? "0" + (date.getMonth() + 1)
        : date.getMonth() + 1;
    var currentDate =
      date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    var hh = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    var mm =
      date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    return (
      date.getFullYear() + "-" + month + "-" + currentDate + " " + hh + ":" + mm
    );
    //返回格式：yyyy-MM-dd hh:mm
  }

  function formatTime(seconds) {
        const sec = seconds % 60; // 计算剩余秒数
        const min = Math.floor(seconds / 60) % 60; // 计算分钟数
        const hours = Math.floor(seconds / (60 * 60)); // 计算小时数
        return `${hours.toString().padStart(2, '0')}:${min.toString().padStart(2, '0')}:${sec.toString().padStart(2, '0')}`;
    }

  function appendTable(data){
        var items = data.auctions.data;
        var count = data.auctions.count;
        var IDs = [];
        var table = "";
        table += "<thead><tr><td>物品</td><td>当前价格</td><td>当前竞拍者ID</td><td>结束时间</td><td>结束剩余时间</td></tr></thead>";
        table += "<tbody>";
        for(var i = 0; i < count; i++){
            // console.log(items[i]);
            var tr = "<tr><td style=\"text-align: center;\">" + items[i].nameFormatted + "</td>";
            tr += "<td style=\"text-align: center;\">" + items[i].price + "</td>";
            //tr += "<td style=\"text-align: center;\">" + (items[i].allianceId === undefined? "-" : items[i].allianceTag)
            //tr += "<td style=\"text-align: center;\"><a target=\"_blank\" href=\"/profile/" + items[i].uid + "\">" + items[i].uid + "</td>";
            tr += "<td style=\"text-align: center;\"><a target=\"_blank\" href=\"/profile/" + items[i].uid_bidder + "\">" + (items[i].uid_bidder === 0 ? "-" : items[i].uid_bidder) + "</td>";
            //tr += "<td style=\"text-align: center;\">" + changeTimeFormat(items[i].time_start * 1000) + "</td>";
            tr += "<td style=\"text-align: center;\">" + changeTimeFormat(items[i].time_end * 1000) + "</td>";
            tr += "<td style=\"text-align: center;\">" + formatTime(items[i].time_end - parseInt(Date.now()/1000)) + "</td>";
            tr += "</tr>"
            table += tr;
            IDs.push(items[i].id)
        }
        table += "</tbody>";
      $("#sellItemInfo").empty().append(table);
    }



  function runOnce() {
      var au = $("#content");
      if(au){
          var table = "<table id=\"sellItemInfo\" style=\"margin-top:10px;margin-bottom:10px;\">";
          table += "</table>";
          au.prepend(table);
          var acution = $("#content");
          var content = acution.html();
          content = content.replace(/ {2,}/g,' ');
          content = content.replace(/\n/g,'');
          var iStart = content.indexOf("HeroAuction.render(") + 19;
          var iEnd = content.indexOf('accounting":') - 2;
          var s = content.substring(iStart, iEnd) + '}';
          var sellingItemArray = JSON.parse(s);
          var sell_auctions = sellingItemArray.sell;
          appendTable(sell_auctions);
        }else{
            setTimeout(runOnce, 5000);
        }
  }

    $(document).ready(function() {
        runOnce();
    });
})();
