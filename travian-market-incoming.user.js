// ==UserScript==
// @name         一键复制来村运输-Travian
// @namespace    https://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @include        https://*.*.*.travian.com/build.php?*&gid=17&t=5
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var button = document.createElement("button");
    button.innerHTML = "一键复制来村运输";
    button.style.position = 'absolute';
    button.style.top = "10px";
    button.style.left = "10px";
    button.style.width = 300;
    button.style.height = 300;
    button.style.padding = '10px';
    button.style.background = '#f0f0f0';
    button.style.display = 'block';
    button.style.zIndex = 99;

    // 2. Append somewhere
    var body = document.getElementsByClassName("contentTitle")[0];
    //body.appendChild(button);
    body.append(button);

    // 3. Add event handler
    button.addEventListener ("click", function() {
        var copyText = "";
        var routes = $($(".routes")[0]).find(".route");
        if (routes.length > 0) {
            routes.each(function(){
                // console.log($(this));
                var aPV = $(this).find(".otherVillage").find("a");
                var playerName = aPV[aPV.length - 1].text ;
                var villageName = aPV[0].text;
                var lumber = $(this).find('.delivery').find("span.value")[0].outerText.replace('.',"");
                var clay = $(this).find('.delivery').find("span.value")[1].outerText.replace('.',"");
                var iron = $(this).find('.delivery').find("span.value")[2].outerText.replace('.',"");
                var crop = $(this).find('.delivery').find("span.value")[3].outerText.replace('.',"");
                var time = $(this).find('.delivery').find("span.time")[0].outerText.replace('.',"");
                var time_interval = $(this).find('.delivery').find("span.timer").attr('id').replace("timer_","");
                copyText += playerName + "\t" + villageName + "\t" + lumber + "\t" + clay + "\t" + iron + "\t" + crop + "\t" + time + "\t" + time_interval + "\n";

            });
            navigator.clipboard.writeText(copyText);
            alert("已复制 "+ routes.length +" 条来村运输");
        }else{
            alert("没有来村运输");
        }
    });
})();