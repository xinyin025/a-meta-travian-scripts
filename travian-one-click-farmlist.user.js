// ==UserScript==
// @name         Travian自动发一键羊单
// @namespace    https://www.btravian.com
// @description 开启后，每隔8分钟，进入羊单，点击一键发送羊单按钮。
// @author       xinyin025
// @version      1.0
// @description  自动点击指定页面上的按钮
// @include        *://*.travian.*
// @include        *://*/*.travian.*
// @exclude     *://*.travian*.*/hilfe.php*
// @exclude     *://*.travian*.*/log*.php*
// @exclude     *://*.travian*.*/index.php*
// @exclude     *://*.travian*.*/anleitung.php*
// @exclude     *://*.travian*.*/impressum.php*
// @exclude     *://*.travian*.*/anmelden.php*
// @exclude     *://*.travian*.*/gutscheine.php*
// @exclude     *://*.travian*.*/spielregeln.php*
// @exclude     *://*.travian*.*/links.php*
// @exclude     *://*.travian*.*/geschichte.php*
// @exclude     *://*.travian*.*/tutorial.php*
// @exclude     *://*.travian*.*/manual.php*
// @exclude     *://*.travian*.*/ajax.php*
// @exclude     *://*.travian*.*/ad/*
// @exclude     *://*.travian*.*/chat/*
// @exclude     *://forum.travian*.*
// @exclude     *://board.travian*.*
// @exclude     *://shop.travian*.*
// @exclude     *://*.travian*.*/activate.php*
// @exclude     *://*.travian*.*/support.php*
// @exclude     *://help.travian*.*
// @exclude     *://*.answers.travian*.*
// @exclude     *.css
// @exclude     *.js
// @grant        GM_registerMenuCommand
// @run-at document-end
// ==/UserScript==

(function() {
    'use strict';

    var useDOMs = typeof window.localStorage == 'undefined' ? false : true;

    function FL_getValue ( key, defaultValue ) {
        if( useDOMs ) {
            var value = window.localStorage.getItem(key);
            if( value == null ) value = defaultValue;
            return value;
        }
        return defaultValue;
    }
    function FL_setValue( key, value ) {
        if( useDOMs ){
            window.localStorage.setItem( key, value );
        }
    }
    function FL_deleteValue( key ) {
        if( useDOMs ){
            window.localStorage.removeItem( key );
        }
    }

    var FL_Task_ID = null;  // 定时器ID

    // 注册开始和停止菜单按钮
    GM_registerMenuCommand("开始", function() {
        if (FL_Task_ID === null) {
            FL_Task_ID = 1;
            FL_setValue("FL_Task_ID",1);
            // setTimeout(Run, 5000);
            console.log("任务已开始");
        }
    });

    GM_registerMenuCommand("停止", function() {
        var FL_Task_ID = FL_getValue("FL_Task_ID",null);
        if (FL_Task_ID !== null) {
            FL_deleteValue("FL_Task_ID");
            FL_Task_ID = null;
            console.log("任务已停止");
        }
    });

    function Run() {
        var FL_Task_ID = FL_getValue("FL_Task_ID",null);
        if(FL_Task_ID !== null){
            var lastExecutionTime = FL_getValue("FL_Task_lastExecutionTime", 0);
            var currentTime = Date.now();

            var sleepDuration = currentTime - lastExecutionTime;
            var count = 1;
            if (sleepDuration < getSleepTime()) {
                var waitTime = getSleepTime() - sleepDuration;
                //console.log("未到发送羊单时间，请等待" + waitTime + "毫秒");
                // 若休眠时间不够，则跳过本次执行
                count = 0;
            }

            if(count == 1){
                //console.log("开始执行发送羊单计划");
                var url = "https://nys.x1.asia.travian.com/build.php?id=39&gid=16&tt=99";
                if(window.location.href !== url){
                    window.location.href = url;
                }else{
                    setTimeout(function() {
                        var button = document.querySelector("#stickyWrapper > button.textButtonV2.buttonFramed.startAllFarmLists.rectangle.withText.green");
                        if (button) {
                            button.click();
                            console.log("已发送羊单");
                        } else {
                            //console.log("找不到发送羊单按钮");
                        }
                        FL_setValue("FL_Task_lastExecutionTime", currentTime);
                    }, 5000);
                }
            }
        }else{
            //console.log("自动羊单任务未开启");
        }
        setTimeout(Run,5000);
    }

    // 获取任务休眠时长（2分钟）
    function getSleepTime() {
        return 8 * 60 * 1000;
    }
    setTimeout(Run,10000);
})();
