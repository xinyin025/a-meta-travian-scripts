# A-Meta-Travian-Scripts

#### 介绍
TR脚本


# 如何安装

1. 浏览器需要支持脚本插件，例如 Tampermonkey, Violentmonkey or Greasemonkey .

2. 点击要安装的插件:

   [File Sell Item](https://gitee.com/xinyin025/a-meta-travian-scripts/raw/master/Find-Sell-item.user.js) 
    > 显示拍卖行自己正在拍卖的物品 

   [Travian Market incoming](https://gitee.com/xinyin025/a-meta-travian-scripts/raw/master/travian-market-incoming.user.js)
    > 一键复制市场中来村运输

   [Travian Attack incoming](https://gitee.com/xinyin025/a-meta-travian-scripts/raw/master/travian-incoming-attack.user.js)
    > 一键复制集结点来村攻击，需要手动点进集结点页（/build.php?id=39&gid=16&tt=1）

   [Travian 自动发一键羊单](https://gitee.com/xinyin025/a-meta-travian-scripts/raw/master/travian-one-click-farmlist.user.js)
    > 每隔8分钟，发送一键羊单
    
3. 插件会自动打开，选择安装即可。
